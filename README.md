# nscurl-demo

This project demonstrates nscurl CICD with GitLab Runner and Xcode 9 on macOS 10.13.

- GitLab Runner (system-mode)
- Xcode 9.0 (9A235)
- macOS 10.13 (High Sierra)
